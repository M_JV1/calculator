#include "CBigDouble.h"
#include<vector>

BigDouble:: BigDouble(double num)
{
    if(num<0)isNegative=true;
    else isNegative=false;

    int Int=num;
    double dec=num-Int;
    vector<int> temp;
    if(Int==0)temp.push_back(0);
    else while(Int!=0)
        {
            temp.push_back(Int%10);
            Int/=10;
        }
    integer=new short int[lenOfInt=temp.size()];
    for(int i=0;i<lenOfInt;i++)
        integer[i]=temp[lenOfInt-1-i];

    temp.erase(temp.begin(),temp.end());
    for(lenOfDec=0;dec!=0;dec=dec*10-(int)(dec*10),lenOfDec++)
        temp.append((int)(dec*10));
    if(lenOfDec!=0)decimal=new short int[lenOfDec];

    for(int i=0;i<lenOfDec;i++)
        decimal[i]=temp[i];
}

ostream& operator<<(ostream& out,BigDouble& num)
{
    if(isNegative)out<<'-';
    for(int i=0;i<lenOfInt;i++)out<<integer[i];
    if(lenOfDec!=0)
    {
        out<<'.';
        for(int i=0;i<lenOfDec;i++)out<<decimal[i];
    }
    return out;
}

istream& operator >>(istream& in ,BigDouble & num)
{
    string numIn;
    in>>numIn;
    if(numIn[0]=='-')
    {
        isNegative=true;
        numIn.erase(0,1);
    }
    else if(numIn[0]=='+')
        {
            isNegative=false;
            numIn.erase(0,1);
        }
    else isNegative=false;
//  check sign
    while(numIn[0]=='0' && numIn[1]!=0) numIn.erase(0,1);//remove 0 in front of number : 000032.5 --> 32.5

    lenOfDec=lenOfInt=0;
    int i;
    for(i=0;numIn[i]!=0&&numIn[i]!='.';i++,lenOfInt++)
        if(numIn[i]<'0'||numIn[i]>'9')throw false;
    integer=new short int[lenOfInt];

    for(i= numIn[i]=='.'?i+1:i;numIn[i]!=0;i++,lenOfDec++)
        if(numIn[i]<'0'||numIn[i]>'9')throw false;

    if(lenOfDec!=0)
    {//remove 0 in end of number : 32.500 --> 32.5
        while(numIn[numIn.size()-1]=='0')
        {
            numIn.erase(numIn.size()-1,1);
            lenOfDec--;
        }
    }
    if(lenOfDec!=0)decimal=new short int[lenOfDec];

    for(i=0;i<lenOfInt;i++)
    {
        integer[i]=numIn[i]-'0';
    }
    int ip=0;
    for(i++;ip<lenOfDec;ip++,i++)
    {
        decimal[ip]=numIn[i]-'0';
    }
    return in;
}
